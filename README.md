FishEye & Crucible REST examples
================================

These scripts should be used with caution as they are not covered by Atlassian technical support.

If you have any questions please do let us know at https://answers.atlassian.com tagged crucible or fisheye.

Installation
======================================

    pip install git+https://bitbucket.org/atlassian/fecru-rest-examples.git

Usage
======================================

See examples_*.py for usage examples.

Step 1 - imports
--------------

    :::python
    from fecru_rest.admin_users import *
    from fecru_rest.admin_groups import *
    from fecru_rest.admin_repositories import *
    from fecru_rest.admin_permission_schemes import *
    from fecru_rest.admin_projects import *
    import fecru_rest.config

Step 2 - configure
--------------

    :::python
    config.url = 'http://localhost:6060/foo'
    config.user = 'admin'
    config.password = 'admin'

Step 3 - call REST API client
--------------

### Users & Groups example

    :::python
    create_user("example_user", "1234", "Example User", "example.user@gmail.com")
    create_group("example_group")
    add_user_to_group("example_user", "example_group")


### Repositories example

    :::python
    new_git = {
        'type' : 'git',
        'name' : 'example_repository_1',
        'description' : "FeCru REST examples repository",
        'storeDiff' : True,
        'enabled' : False,
        'git' : {
            'location' : "git@bitbucket.org:atlassian/fecru-rest-examples.git",
            'auth' : {
                'authType' : 'key-generate'
            }
        }
    }
    create_repository(new_git)

### Permission scheme example

    :::python
    create_permission_scheme("example-permission")
    add_user_action("example-permission", "example_user", "action:viewReview")
    add_user_action("example-permission", "example_user", "action:createReview")
    add_group_action("example-permission", "example_group", "action:approveReview")
    add_group_action("example-permission", "example_group", "action:rejectReview")
    add_review_role_action("example-permission", "reviewer", "action:approveReview")
    add_review_role_action("example-permission", "reviewer", "action:rejectReview")
    add_logged_in_users_action("example-permission", "action:rejectReview")
    add_logged_in_users_action("example-permission", "action:summarizeReview")


### Project example

    :::python
     new_example_project = {
            'key' : 'EXAMPLE-PROJ',
            'name' : 'Example project',
            'defaultRepositoryName' : 'example_repository_1',
            'storeFileContentInReview' : True,
            'permissionSchemeName' : 'example-permission',
            'moderatorEnabled' : True,
            'defaultModerator' : 'example_user',
            'allowReviewersToJoin' : True,
            'defaultDurationInWeekDays' : 5,
            'defaultObjectives' : 'Please make sure feature branch builds are green.'
     }
     create_project(new_example_project)
     add_default_reviewer_group("EXAMPLE-PROJ", "example_group")
     add_allowed_reviewer_user("EXAMPLE-PROJ", "example_user")


Content
======================================

- fecru_rest/main.py - usage examples
- fecru_rest/config.py - configuration
- fecru_rest/admin_groups.py - client API for accessing /admin/groups endpoint
- fecru_rest/admin_users.py - client API for accessing /admin/users endpoint
- fecru_rest/admin_repositories.py - client API for accessing /admin/repositories endpoint
- fecru_rest/admin_permission_schemes.py - client API for accessing /admin/permission-schemes endpoint
- fecru_rest/admin_projects.py - client API for accessing /admin/projects endpoint
- fecru_rest/examples_users_groups.py - examples of using admin_users and admin_groups API
- fecru_rest/examples_repositories.py - examples of using admin_repositories API
- fecru_rest/examples_permission_schemes.py - examples of using admin_permission_schemes API
- fecru_rest/examples_projects.py - examples of using admin_projects API
