import requests
import json
import config

from .common import *
from requests.exceptions import RequestException

# FishEye & Crucible example API for accessing /rest-service-fecru/admin/projects endpoint
# See https://docs.atlassian.com/fisheye-crucible/latest/wadl/fecru.html#rest-service-fecru:admin:projects for FeCru Projects REST API docs.

def get_projects(start=0, limit=100):
    return get_all_paged_results("admin/projects", start, limit)


def get_project(project_key):
    path = "admin/projects/%s" % project_key
    r = requests.get(url(path), auth=(config.user, config.password))
    assert(r.status_code == 200)
    return r.json()


def create_project(new_project):
    path = "admin/projects"
    headers = {'content-type': 'application/json'}
    r = requests.post(url(path), data=json.dumps(new_project), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 201)
    return r.json()


def update_project(project_key, update_project):
    path = "admin/projects/%s" % project_key
    headers = {'content-type': 'application/json'}
    r = requests.put(url(path), data=json.dumps(update_project), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 200)
    return r.json()

def delete_project(name, delete_project_reviews=False):
    path = "admin/projects/%s?deleteProjectReviews=%s" % (name, delete_project_reviews)
    r = requests.delete(url(path), auth=(config.user, config.password))
    return r.status_code == 204

def try_delete_project(name):
    try:
        delete_project(name, True)
    except RequestException:
        pass


def __add_name_to_project_list(project_key, list, name):
    path = "admin/projects/%s/%s" % (project_key, list)
    payload = {'name': name}
    headers = {'content-type': 'application/json'}
    r = requests.put(url(path), data=json.dumps(payload), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 204)


def __delete_name_from_project_list(project_key, list, name):
    path = "admin/projects/%s/%s" % (project_key, list)
    payload = {'name': name}
    headers = {'content-type': 'application/json'}
    r = requests.delete(url(path), data=json.dumps(payload), headers=headers, auth=(config.user, config.password))
    return r.status_code == 204


def __get_names_from_project_list(project_key, list):
    path = "admin/projects/%s/%s" % (project_key, list)
    return get_all_paged_results(path)


def add_default_reviewer_user(project_key, user_name):
    return __add_name_to_project_list(project_key, "default-reviewer-users", user_name)


def delete_default_reviewer_user(project_key, user_name):
    return __delete_name_from_project_list(project_key, "default-reviewer-users", user_name)


def get_default_reviewer_users(project_key):
    return __get_names_from_project_list(project_key, "default-reviewer-users")


def add_default_reviewer_group(project_key, group_name):
    return __add_name_to_project_list(project_key, "default-reviewer-groups", group_name)


def delete_default_reviewer_group(project_key, group_name):
    return __delete_name_from_project_list(project_key, "default-reviewer-groups", group_name)


def get_default_reviewer_groups(project_key):
    return __get_names_from_project_list(project_key, "default-reviewer-groups")


def add_allowed_reviewer_user(project_key, user_name):
    return __add_name_to_project_list(project_key, "allowed-reviewer-users", user_name)


def delete_allowed_reviewer_user(project_key, user_name):
    return __delete_name_from_project_list(project_key, "allowed-reviewer-users", user_name)


def get_allowed_reviewer_users(project_key):
    return __get_names_from_project_list(project_key, "allowed-reviewer-users")


def add_allowed_reviewer_group(project_key, group_name):
    return __add_name_to_project_list(project_key, "allowed-reviewer-groups", group_name)


def delete_allowed_reviewer_group(project_key, group_name):
    return __delete_name_from_project_list(project_key, "allowed-reviewer-groups", group_name)


def get_allowed_reviewer_groups(project_key):
    return __get_names_from_project_list(project_key, "allowed-reviewer-groups")


def move_reviews(from_project_key, to_project_key):
    path = "admin/projects/%s/move-reviews/%s" % (from_project_key, to_project_key)
    headers = {'content-type': 'application/json'}
    r = requests.put(url(path), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 204)
