from .admin_permission_schemes import *


# FishEye & Crucible REST examples
# Permission scheme examples
def permissionSchemesExample():

    example_permission_scheme_name_1 = 'example-permission-scheme1'
    example_permission_scheme_name_2 = 'example-permission-scheme2'

    print "Try delete permission scheme: %s" % example_permission_scheme_name_1
    try_delete_permission_scheme(example_permission_scheme_name_1)

    print "Try delete permission scheme: %s" % example_permission_scheme_name_2
    try_delete_permission_scheme(example_permission_scheme_name_2)

    # Get all permission schemes and print:
    # * number of permission schemes
    # * fist 5 permission schemes names
    permission_schemes = get_permission_schemes()
    print "Number of permission schemes: %d" % (len(permission_schemes))
    print "First 5 permission schemes: %s" % (", ".join(sorted(filter(None, map(lambda permission_scheme: permission_scheme.get('name'), permission_schemes)), key=lambda s: s.lower())[:5]))

    # Create permission scheme
    new_example_permission_scheme = {
        'name' : example_permission_scheme_name_1
    }

    print "Creating permission scheme: %s from agile" % new_example_permission_scheme
    create_permission_scheme(new_example_permission_scheme, "agile")

    print "First 5 permission schemes: %s" % (", ".join(sorted(filter(None, map(lambda permission_scheme: permission_scheme.get('name'), get_permission_schemes())), key=lambda s: s.lower())[:5]))

    update_example_permission_scheme = {
        'name' : example_permission_scheme_name_2
    }

    print "Update permission scheme: %s with %s" % (example_permission_scheme_name_1, update_example_permission_scheme)
    update_permission_scheme(example_permission_scheme_name_1, update_example_permission_scheme)

    print "First 5 permission schemes: %s" % (", ".join(sorted(filter(None, map(lambda permission_scheme: permission_scheme.get('name'), get_permission_schemes())), key=lambda s: s.lower())[:5]))

    print "Configuring permission scheme: %s for user: %s actions" % (example_permission_scheme_name_2, "example_user")
    add_user_action(example_permission_scheme_name_2, "example_user", "action:viewReview")
    add_user_action(example_permission_scheme_name_2, "example_user", "action:createReview")
    add_user_action(example_permission_scheme_name_2, "example_user", "action:abandonReview")

    print "Permission scheme users actions: %s" % (get_users_actions(example_permission_scheme_name_2))

    print "Configuring permission scheme: %s for group: %s actions" % (example_permission_scheme_name_2, "example_group1")
    add_group_action(example_permission_scheme_name_2, "example_group1", "action:approveReview")
    add_group_action(example_permission_scheme_name_2, "example_group1", "action:rejectReview")
    add_group_action(example_permission_scheme_name_2, "example_group1", "action:summarizeReview")

    print "Permission scheme groups actions: %s" % (get_groups_actions(example_permission_scheme_name_2))

    print "Configuring permission scheme: %s for review-roles: %s actions" % (example_permission_scheme_name_2, "author")
    add_review_role_action(example_permission_scheme_name_2, "reviewer", "action:approveReview")
    add_review_role_action(example_permission_scheme_name_2, "reviewer", "action:rejectReview")
    add_review_role_action(example_permission_scheme_name_2, "reviewer", "action:summarizeReview")

    print "Permission scheme review-roles actions: %s" % (get_review_role_actions(example_permission_scheme_name_2))

    print "Configuring permission scheme: %s for logged-in-users actions" % (example_permission_scheme_name_2)
    add_logged_in_users_action(example_permission_scheme_name_2, "action:approveReview")
    add_logged_in_users_action(example_permission_scheme_name_2, "action:rejectReview")
    add_logged_in_users_action(example_permission_scheme_name_2, "action:summarizeReview")

    print "Permission scheme logged-in-users actions: %s" % (get_logged_in_users_actions(example_permission_scheme_name_2))

    print "Permission scheme anonymous-users actions: %s" % (get_anonymous_users_actions(example_permission_scheme_name_2))

    print "Configuring permission scheme: %s for anonymous-users actions" % (example_permission_scheme_name_2)
    delete_anonymous_users_action(example_permission_scheme_name_2, "action:viewReview")

    print "Permission scheme anonymous-users actions: %s" % (get_anonymous_users_actions(example_permission_scheme_name_2))


    print "Done permission schemes examples."




